from collections import deque
import csv
import os
from sys import argv, exit

EXPS = int()

try:
    EXPS = int(argv[1])
except:
    print "Give the number of experiments as parameter"
    raise SystemExit

arquivos = [ f for f in os.listdir('../') if f.startswith('best_worse_average_repetitions_') ]

best = 0
worse = 0
average = 0
repetitions = 0
variability = 0

for arquivo in arquivos:
    with open('../{}'.format(arquivo), 'rb') as statistics_file:
        reader = csv.reader(statistics_file, delimiter=';')

        row = deque(reader, 1)[0]
        best += float(row[0])
        worse += float(row[1])
        average += float(row[2])
        repetitions += int(row[3])
        variability += int(row[4])
        invalid_solutions += int(row[5])

print "{}\t{}\t{}\t{}\t{}\t{}".format(best/EXPS, worse/EXPS, average/EXPS, repetitions/EXPS, variability/EXPS, invalid_solutions)
