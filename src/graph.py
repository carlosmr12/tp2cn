# -*- coding: utf-8 -*-
from collections import defaultdict

import config

class Ant(object):
    def __init__(self):
        self.invalid = False
        self.attempt = 0
        self.path = list()
        self.visited_nodes = list()

    @property
    def total_weight(self):
        total_weight = 0
        for edge in self.path:
            total_weight += edge.weight
        return total_weight

class Edge(object):
    def __init__(self, node_to, weight):
        self.node_to = node_to
        self.weight = weight
        self.feromone = 1

    @property
    def quality(self):
        return self.weight*self.feromone

class Graph(object):
    def __init__(self, number_nodes, connections, directed=False):
        self._graph = defaultdict(list)
        self.number_nodes = number_nodes
        self._directed = directed
        self.edges = list()
        self.add_connections(connections)

    def quality_all_neighbors(self, node):
        total_quality = 0
        for neighbor in self._graph[node]:
            total_quality += neighbor.quality
        return total_quality

    def add_connections(self, connections):
        for node1,node2,weight in connections:
            self.add(node1, node2, weight)

    def add(self, node1, node2, weight):
        edge1 = Edge(node2, weight)
        self._graph[node1].append(edge1)
        self.edges.append(edge1)
        if not self._directed:
            edge2 = Edge(node1, weight)
            self._graph[node2].append(edge2)
            self.edges.append(edge2)

    def evaporateFeromone(self):
        for edge in self.edges:
            edge.feromone = (1 - config.EVAPORATION_RATE)*edge.feromone
        return True
