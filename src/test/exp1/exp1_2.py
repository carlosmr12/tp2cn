# -*- coding: utf-8 -*-
from matplotlib import pyplot
import numpy as np
import csv
import os

arquivos = [ f for f in os.listdir('.') if os.path.isfile(os.path.join('.',f)) and f.endswith('.csv') ]

for arquivo in arquivos:
    y_data = []

    with open(arquivo, 'rb') as f:
        reader = csv.reader(f, delimiter=";")

        for row in reader:
            y_data.append(int(row[3]))
        pyplot.plot(y_data, label=arquivo)

pyplot.ylim([0,75])
pyplot.title(u"Experimento 1")
pyplot.xlabel(u"Épocas")
pyplot.ylabel(u"Quantidade de soluções repetidas")
pyplot.legend(bbox_to_anchor=(1,1))

pyplot.savefig('exp1_2.png')
