# -*- coding: utf-8 -*-
from matplotlib import pyplot
import csv
import os

arquivos = [ f for f in os.listdir('.') if os.path.isfile(os.path.join('.',f)) and f.endswith('.csv') ]

for arquivo in arquivos:
    y_data = []

    with open(arquivo, 'rb') as f:
        reader = csv.reader(f, delimiter=";")

        for row in reader:
            y_data.append(int(row[0]))
        pyplot.plot(y_data, label=arquivo)

pyplot.title(u"Experimento 1")
pyplot.xlabel(u"Épocas")
pyplot.ylabel(u"Melhores soluções")
pyplot.legend(bbox_to_anchor=(1,0.5))

pyplot.savefig('exp1.png')
