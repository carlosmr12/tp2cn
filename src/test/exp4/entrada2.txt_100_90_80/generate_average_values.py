from collections import deque
import csv
import os
from sys import argv, exit

EPOCHS = int()

try:
    EPOCHS = int(argv[1])
except:
    print "Give the number of epochs as parameter"
    raise SystemExit

arquivos = [ f for f in os.listdir('.') if f.startswith('best_worse_average_repetitions_') ]

rows = []

for arquivo in arquivos:
    with open('{}'.format(arquivo), 'rb') as statistics_file:
        reader = csv.reader(statistics_file, delimiter=';')
        count = 0
        for line in reader:
            try:
                rows[count] = [x + y for x, y in zip(rows[count], map(int, line))]
            except IndexError:
                rows.append(map(int, line))
            count += 1

with open('average_stats.csv', 'wb') as average_stats_file:
    writer = csv.writer(average_stats_file, delimiter=';')
    for line in [map(lambda x: x/len(arquivos), line) for line in rows]:
        writer.writerow(line)
