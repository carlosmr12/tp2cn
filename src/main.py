# -*- coding: utf-8 -*-

"""
TP2 – ACO Aplicado ao Problema Longest Path
====================================================================

Carlos Henrique Miranda Rodrigues
chmrodrigues@ufmg.br

DCC831 – Tópicos Especiais em Ciência da Computação - Computação Natural
Departamento de Ciência da Computação
Universidade Federal de Minas Gerais - UFMG
"""
from argparse import ArgumentParser
from time import time
import csv
import numpy as np
import os
import random

from graph import Graph, Ant, Edge
import config

def getRepetitions(list_int):
    list_int_aux = []

    count = 0
    for sublist in list_int:
        if sublist not in list_int_aux and list_int.count(sublist) > 1:
           count += list_int.count(sublist)
           list_int_aux.append(sublist)
    return count

def getNumberOfDifferentSolutions(list_int):
    list_int_aux = []

    count = 0
    for sublist in list_int:
        if sublist not in list_int_aux and list_int.count(sublist) > 1:
           count += 1
           list_int_aux.append(sublist)
    return count

def getStats(ants):
    total_weights = [item.total_weight for item in ants]
    best = max(total_weights)
    worse = min(total_weights)
    average = sum(total_weights)/len(total_weights)
    repetitions = getRepetitions(total_weights)
    different_solutions = getNumberOfDifferentSolutions(total_weights)
    invalid_solutions = len([item for item in ants if item.invalid == True])
    return [best, worse, average, repetitions, different_solutions, invalid_solutions]

def main():
    connections = []
    config.NUMBER_NODES = int()
    config.ORIGIN = int()
    config.END = int()

    with open('{}'.format(config.INPUT_FILE), 'r') as f:
        config.NUMBER_NODES = int(f.readline())
        path = f.readline()
        config.ORIGIN = int(path.split()[0])
        config.END = int(path.split()[1])

        for line in f:
            connection = line.split()
            connections.append(tuple([int(item) for item in connection]))

    graph = Graph(config.NUMBER_NODES, connections, directed=True)

    best_solution = Ant()
    ants = []
    ants.append(best_solution)
    for i in range(0, config.NUMBER_ANTS-1):
        ant = Ant()
        ants.append(ant)

    for epoch in range(1, config.EPOCHS + 1):
        for ant in ants:
            if ant.visited_nodes == []:
                vertex = config.ORIGIN
                ant.visited_nodes.append(vertex)
                while vertex != config.END:
                    valid_neighbors = [n for n in graph._graph[vertex] if n.node_to not in ant.visited_nodes and n.quality > 0]

                    # Caminho sem volta - Todos os vizinhos já estão na solução
                    if valid_neighbors == []:
                        if ant.attempt < 3:
                            while ant.attempt < 3:
                                ant.attempt += 1
                                ant.path = ant.path[:-len(ant.path)/2]
                                ant.visited_nodes = ant.visited_nodes[:-len(ant.visited_nodes)/2]
                                vertex = ant.visited_nodes[-1]
                        else:
                            ant.invalid = True
                            break

                    else:
                        probs = []

                        for edge in valid_neighbors:
                            probs.append(round(float(edge.quality)/graph.quality_all_neighbors(vertex), 6))

                        # Normalização das probabilidades
                        try:
                            if sum(probs) != 1:
                                probs[:] = [prob/sum(probs) for prob in probs]
                            edge = np.random.choice(valid_neighbors, p=probs)
                        except ZeroDivisionError:
                            # Caminho sem volta todas as probabilidades são 0
                            edge = random.choice(valid_neighbors)

                        vertex = edge.node_to
                        ant.visited_nodes.append(edge.node_to)
                        ant.path.append(edge)

            if ant.total_weight > best_solution.total_weight and not ant.invalid:
                best_solution = ant

        for ant in ants:
            if ant is best_solution:
                for edge in ant.path:
                    edge.feromone += (2*(1 - float(1)/ant.total_weight))
            else:
                if ant.invalid:
                    for edge in ant.path:
                        edge.feromone += float(1)/ant.total_weight
                else:
                    for edge in ant.path:
                        edge.feromone += (1 - float(1)/ant.total_weight)
        graph.evaporateFeromone()

        with open('{}/solutions_{}_{}_{}_{}.csv'.format(config.DIR, config.EPOCHS, config.NUMBER_ANTS, config.EVAPORATION_RATE*100, os.getpid()), 'ab') as stats_file:
            writer = csv.writer(stats_file, delimiter=';')
            for ant in ants:
                writer.writerow(ant.visited_nodes)

        with open('{}/total_weights_{}_{}_{}_{}.csv'.format(config.DIR, config.EPOCHS, config.NUMBER_ANTS, config.EVAPORATION_RATE*100, os.getpid()), 'ab') as stats_file:
            writer = csv.writer(stats_file, delimiter=';')
            writer.writerow([item.total_weight for item in ants])

        with open('{}/best_worse_average_repetitions_{}_{}_{}_{}.csv'.format(config.DIR, config.EPOCHS, config.NUMBER_ANTS, config.EVAPORATION_RATE*100, os.getpid()), 'ab') as stats_file:
            writer = csv.writer(stats_file, delimiter=';')
            writer.writerow(getStats(ants))

        print best_solution.visited_nodes
        print '\n'.join(str(item) for item in getStats(ants)) 

        del ants[:]
        ants = []
        ants.append(best_solution)
        for i in range(0, config.NUMBER_ANTS-1):
            ant = Ant()
            ants.append(ant)

if __name__ == "__main__":
    parser = ArgumentParser(description=u'TP2 - Abordagem Baseada em ACO para solução do Problema Longest Path - DCC831')

    parser.add_argument('number_ants', metavar='number_ants', default=90, nargs='?', type=int, help=u'Number of ants that will be used by the ACO')
    parser.add_argument('epochs', metavar='epochs', default=100, nargs='?', type=int, help=u'Number of iterations on the ACO')
    parser.add_argument('evaporation_rate', metavar='evaporation_rate', default=0.6, nargs='?', type=float, help=u'Pheromone evaporation rate')
    parser.add_argument('input_file', metavar='input_file', default='fixtures/entrada2.txt', nargs='?', type=str, help=u'Input file with graph definitions')

    args = parser.parse_args()

    config.NUMBER_ANTS = args.number_ants
    config.EPOCHS = args.epochs
    config.EVAPORATION_RATE = args.evaporation_rate
    config.INPUT_FILE = args.input_file

    config.DIR = '{}_{}_{}_{}'.format(config.INPUT_FILE.split('/')[1], config.EPOCHS, config.NUMBER_ANTS, int(config.EVAPORATION_RATE*100))
    if not os.path.exists(config.DIR):
        os.makedirs(config.DIR)

    main()
