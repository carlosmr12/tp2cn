TP2 – ACO Aplicado ao Problema Longest Path
===========================================

Carlos Henrique Miranda Rodrigues
chmrodrigues@ufmg.br

DCC831 – Tópicos Especiais em Ciência da Computação - Computação Natural
Departamento de Ciência da Computação
Universidade Federal de Minas Gerais - UFMG

###Requirements
```pip install -r requirements.txt```

###Run
```python main.py epochs number_of_ants evaporation_rate```

All parameters are optional. If not provided the following default values will be used:
: epochs - 100
: number_of_ants - 90
: evaporation_rate - 0.4
